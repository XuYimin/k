﻿# -*- coding: utf-8 -*-

import numpy as np
import pandas as pd
import tushare as ts
import talib
import time
import easytrader

def AvgDiff(stock, level, sa = 5, la = 20):
    k_ohlc = ts.get_k_data(stock, ktype = level)
    k = np.array(k_ohlc['close'])  
    SA = talib.MA(k, sa, matype = 0)
    LA = talib.MA(k, la, matype = 0)
    return (SA[-1] - LA[-1]) / LA[-1]

def updateStockPool(level = 'd', sa = 5, la = 20):
    hs = ts.get_hs300s()
    newc = hs.columns.insert(4, 'AvgDiff')  
    hs = hs.reindex(columns = newc)
    for i in hs.index:
        hs.loc[i, 'AvgDiff'] = AvgDiff(hs.loc[i,'code'], level, sa, la)
    hs = hs.sort_values('AvgDiff')
    hs.to_csv('hs.csv')
    print('Stock Pool Updated')
    
def getAvg(stock, level):
    k_ohlc = ts.get_k_data(stock, ktype = level)
    k = np.array(k_ohlc['close'])  
    SA = talib.MA(k, 5, matype = 0)
    MA = talib.MA(k, 10, matype = 0)
    LA = talib.MA(k, 20, matype = 0)
    return SA[-2], SA[-1], MA[-2], MA[-1], LA[-2], LA[-1]    
    
def getBuySellPrice(stock):
    k5 = ts.get_k_data(stock, ktype = '5')
    k5_now = k5.ix[k5.index[-1]]
    return k5_now['high'], k5_now['low'], k5_now['close']  
                
class Trade(object):    
    def __init__(self):
        self.status = 0
        
    def login(self):
        self.user = easytrader.use('yh_client')
        self.user.prepare(user = '311900001928', password = '161116')
        self.status = 1
    
    def getBalance(self):
        b = self.user.balance()
        return b['可用金额']

    # 根据证券名称返回用户持仓中对应的查询项目
    def getPosition(self, stock, resName = '可用余额'):
        if self.status == 0:
            return 0
        
        try:
            P = self.user.position
            for p in P:
                if int(stock) == int(p['证券代码']):
                    return p[resName]  
            return 0
        except:
            print('Position Error')
            self.login()
            time.sleep(10)
            return self.getPosition(stock, resName)
        
t = Trade()

class AdjustPostion(object):
    def __init__(self, row):
        self.row = row
        self.level = str(row['级别'])
        code = str(row['代码'])
        self.code = (6 - len(code)) * '0' + code
        self.sa2, self.sa, self.ma2, self.ma, self.la2, self.la \
        = self.getAvg(self.level)   
        self.avg5 = self.getAvg('5')
        self.k5 = getBuySellPrice(self.code)
    
    def write(self, info, txt = 'log.txt'):
        with open(txt, 'w+') as f:
            f.write(info)
        
    def getAvg(self, level):
        return getAvg(self.code, level)
    
    def getSubLevel(self):
        if self.level == 'w':
            return 'd'
        if self.level == 'd':
            return '30'
    
    # 判断操作确立状态
    def update(self):
        if self.row['建仓状态'] == 1:
            if self.sa / self.ma > 1.01:
                self.row['建仓状态'] = 2  
                        
        if self.row['加仓状态'] == 1:
            if self.sa / self.la > 1.01:
                self.row['加仓状态'] = 2 
        
        if self.row['补仓状态'] == 1:
            sublevel = self.getSubLevel()
            sa2, sa, ma2, ma, la2, la = self.getAvg(sublevel)
            if sa / ma > 1.01:
                self.row['补仓状态'] = 2
            
    def openPosition(self):
        # 已建仓,忽略
        if self.row['建仓状态']!= 0:
            return
        
        # 等待 5 分钟下跌趋势结束
        sa2, sa, ma2, ma, la2, la = self.avg5
        if sa < ma:
            return
        
        high, low, close = self.k5
        
        amountx = self.row['建仓'] 
        if self.row['底仓状态'] == 0:
            amountx += self.row['底仓']
        
        if (self.sa / self.ma) > 0.995 > (self.sa2 / self.ma2): 
            self.row['建仓止损'] = 0.97 * high
            if t.status == 1:
                b = t.getBalance()
                if b > high * amountx:
                    t.user.buy(self.code, high, amountx)
                    self.row['建仓状态'] = 1
                    self.row['底仓状态'] = 1  
                else:
                    print('可用金额不足')
            print(time.strftime('%Y-%m-%d %H:%M:%S', \
                    time.localtime(time.time())))
            print('建仓', self.row['名称'], amountx, high)
    
    def closePosition(self):
        # 未建仓，忽略
        if self.row['建仓状态'] == 0:
            return 
        
        # 等待 5 分钟上升趋势结束
        sa2, sa, ma2, ma, la2, la = self.avg5
        if sa > ma:
            return
        
        high, low, close = self.k5
        amount = self.row['建仓']
        amount_Available = t.getPosition(self.code)
        if amount  > amount_Available:
            print(self.row['名称'], '建仓可卖数量不足')
            return
        
        # 强制止损线
        if close < self.row['建仓止损']:
            if t.status == 1:
                t.user.sell(self.code, low, amount)
                self.row['建仓状态'] = 0
            print(time.strftime('%Y-%m-%d %H:%M:%S', \
                    time.localtime(time.time())))
            print('建仓硬止损', self.row['名称'], amount, low)
        
        # 止损线
        if self.row['建仓状态'] == 1:
            if self.sa / self.ma < 0.985:
                if t.status == 1:
                    t.user.sell(self.code, low, amount)
                    self.row['建仓状态'] = 0
                print(time.strftime('%Y-%m-%d %H:%M:%S', \
                    time.localtime(time.time())))
                print('建仓动止损', self.row['名称'], amount, low)
        
        # 止盈线
        if self.row['建仓状态'] == 2:
            if self.sa / self.ma < 1.005:
                if t.status == 1:
                    t.user.sell(self.code, low, amount)
                    self.row['建仓状态'] = 0
                print(time.strftime('%Y-%m-%d %H:%M:%S', \
                    time.localtime(time.time())))
                print('建仓止盈', self.row['名称'], amount, low)

    def addPosition(self):
        # 已加仓,忽略
        if self.row['加仓状态']!= 0:
            return
        
        # 等待 5 分钟下跌趋势结束
        sa2, sa, ma2, ma, la2, la = self.avg5
        if sa < ma:
            return
        
        high, low, close = self.k5
        
        amount = self.row['加仓'] 
        
        if (self.sa / self.la) > 0.995 > (self.sa2 / self.la2):
            self.row['加仓止损'] = 0.97 * high
            if t.status == 1:
                b = t.getBalance()
                if b > high * amount:
                    t.user.buy(self.code, high, amount)
                    self.row['加仓状态'] = 1 
                else:
                    print('可用金额不足')
            print(time.strftime('%Y-%m-%d %H:%M:%S', \
                    time.localtime(time.time())))
            print('加仓', self.row['名称'], amount, high)            

    def subtractPosition(self):
        # 未加仓，忽略
        if self.row['加仓状态'] == 0:
            return 
        
        # 等待 5 分钟上升趋势结束
        sa2, sa, ma2, ma, la2, la = self.avg5
        if sa > ma:
            return
        
        high, low, close = self.k5
        amount = self.row['加仓']
        amount_Available = t.getPosition(self.code)
        if amount  > amount_Available:
            print(self.row['名称'], '加仓可卖数量不足')
            return
        
        # 强制止损线
        if close < self.row['加仓止损']:
            if t.status == 1:
                t.user.sell(self.code, low, amount)
                self.row['加仓状态'] = 0
            print(time.strftime('%Y-%m-%d %H:%M:%S', \
                    time.localtime(time.time())))
            print('加仓硬止损', self.row['名称'], amount, low)
        
        # 止损线
        if self.row['加仓状态'] == 1:
            if self.sa / self.la < 0.985:
                if t.status == 1:
                    t.user.sell(self.code, low, amount)
                    self.row['加仓状态'] = 0
                print(time.strftime('%Y-%m-%d %H:%M:%S', \
                    time.localtime(time.time())))
                print('加仓动止损', self.row['名称'], amount, low)
        
        # 止盈线
        if self.row['加仓状态'] == 2:
            if self.sa / self.la < 1.005:
                if t.status == 1:
                    t.user.sell(self.code, low, amount)
                    self.row['加仓状态'] = 0
                print(time.strftime('%Y-%m-%d %H:%M:%S', \
                    time.localtime(time.time())))
                print('加仓止盈', self.row['名称'], amount, low) 
                
    def upPosition(self):
        if self.level == '30':
            return
        
        # 已补仓,忽略
        if self.row['补仓状态']!= 0:
            return
        
        # 只在本级别的多头环境下进行补仓操作
        if not(self.sa > self.ma > self.la):
            return
        
        # 等待 5 分钟下跌趋势结束
        sa2, sa, ma2, ma, la2, la = self.avg5
        if sa < ma:
            return
        
        sublevel = self.getSubLevel()
        sa2, sa, ma2, ma, la2, la = self.getAvg(sublevel)
        
        high, low, close = self.k5
        amount = self.row['补仓'] 
        
        if (sa / ma) > 0.995 > (sa2 / ma2):
            self.row['补仓止损'] = 0.97 * high
            if t.status == 1:
                b = t.getBalance()
                if b > high * amount:
                    t.user.buy(self.code, high, amount)
                    self.row['补仓状态'] = 1 
                else:
                    print('可用金额不足')
            print(time.strftime('%Y-%m-%d %H:%M:%S', \
                    time.localtime(time.time())))
            print('补仓', self.row['名称'], amount, high)            

    def downPosition(self):
        if self.level == '30':
            return
        
        # 未补仓，忽略
        if self.row['补仓状态'] == 0:
            return 
        
        # 等待 5 分钟上升趋势结束
        sa2, sa, ma2, ma, la2, la = self.avg5
        if sa > ma:
            return
        
        sublevel = self.getSubLevel()
        sa2, sa, ma2, ma, la2, la = self.getAvg(sublevel)
        
        high, low, close = self.k5
        amount = self.row['补仓']
        amount_Available = t.getPosition(self.code)
        if amount  > amount_Available:
            print(self.row['名称'], '补仓可卖数量不足')
            return
        
        # 硬止损
        if close < self.row['补仓止损']:
            if t.status == 1:
                t.user.sell(self.code, low, amount)
                self.row['补仓状态'] = 0
            print(time.strftime('%Y-%m-%d %H:%M:%S', \
                    time.localtime(time.time())))
            print('补仓硬止损', self.row['名称'], amount, low)
        
        # 止损线
        if self.row['补仓状态'] == 1:
            if sa / ma < 0.985:
                if t.status == 1:
                    t.user.sell(self.code, low, amount)
                    self.row['补仓状态'] = 0
                print(time.strftime('%Y-%m-%d %H:%M:%S', \
                    time.localtime(time.time())))
                print('补仓动止损', self.row['名称'], amount, low)
        
        # 止盈线
        if self.row['补仓状态'] == 2:
            if sa / ma < 1.005:
                if t.status == 1:
                    t.user.sell(self.code, low, amount)
                    self.row['补仓状态'] = 0
                print(time.strftime('%Y-%m-%d %H:%M:%S', \
                    time.localtime(time.time())))
                print('补仓止盈', self.row['名称'], amount, low)  
                
    def calc(self):            
        self.update()
        self.openPosition()
        self.closePosition()
        self.addPosition()
        self.subtractPosition()
        self.upPosition()
        self.downPosition()

class P(object):
    def __init__(self):
        self.p = self.read()
    
    def read(self, csv = 'p.csv'):
        return pd.read_csv(csv, encoding = 'gb2312')
        
    def write(self, csv = 'p.csv'):
        self.p.to_csv(csv)     
    
    def updateRow(self):
        p = self.p
        for i, row in p.iterrows():
            ap = AdjustPostion(row)
            ap.calc()
            self.p = self.p.drop(i)
            self.p = self.p.append(ap.row)
# 主函数    
def mainx():
    p = P()
    
    while True:   
        tm = time.localtime(time.time())
        h = tm.tm_hour
        m = tm.tm_min
        if (h < 9) or (h == 9 and m <= 30) or (h == 11 and m >= 30) \
            or (h == 12) or (h >= 15):
            time.sleep(10)
            continue

        p.updateRow()
        p.write()
        time.sleep(10)

def main():
    try:
        t.login()
        mainx()
    except:
        main()
    